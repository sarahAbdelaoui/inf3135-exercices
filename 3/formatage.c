#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int i;
    printf("Nombre     Entier    Octal    Hexa\n");
    printf("------     ------    -----    ----\n");
    for (i = 1; i < argc; ++i) {
        char *reste;
        float f = strtof(argv[i], &reste);
        if (reste == argv[1]) {
            printf("ERREUR\n");
            return 1;
        } else {
            int i = (int)f;
            printf("%8.4f   %-4d      %-4o     %-4x   \n", f, i, i, i);
        }
    }
    return 0;
}

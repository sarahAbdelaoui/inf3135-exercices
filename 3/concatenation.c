#include <stdio.h>
#include <string.h>

/**
 * Concatène plusieurs chaînes de caractères et place
 * le résultat dans ``resultat``.
 */
void concatenerPlusieurs(const char *chaines[],
                         int nbChaines,
                         char *resultat,
                         int capacite) {
    int i = 0;
    int j = 0;
    char *ps = resultat;
    while (i < capacite - 1 && j < nbChaines) {
        int lngChaine = strlen(chaines[j]);
        strncpy(ps, chaines[j], capacite - i - 1);
        i += lngChaine;
        ps += lngChaine;
        ++j;
    }
}

int main() {
    const char *chaines[] = {"alpha ", "beta ", "gamma"};
    char resultat[80];
    concatenerPlusieurs(chaines, 3, resultat, 80);
    printf("|%s|\n", resultat);
}

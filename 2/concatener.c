#include <stdio.h>

/**
 * Concatène les chaînes s et t dans resultat.
 */
void concatener(const char s[], const char t[], char resultat[]) {
    int i = 0, j = 0, k = 0;
    while (s[i] != '\0') {
        resultat[k] = s[i];
        ++i;
        ++k;
    }
    while (t[j] != '\0') {
        resultat[k] = t[j];
        ++j;
        ++k;
    }
    resultat[k] = '\0';
}

/**
 * Concatène les chaînes s et t dans resultat
 * (version compacte).
 */
void concatener2(const char s[], const char t[], char resultat[]) {
    int i = 0, j = 0, k = 0;
    while (s[i] != '\0') resultat[k++] = s[i++];
    while (t[j] != '\0') resultat[k++] = t[j++];
    resultat[k] = '\0';
}

int main() {
    char chaine[80];
    concatener("Première chaîne", "Deuxième chaîne", chaine);
    printf("%s\n", chaine);
    concatener2("Première chaîne 2", "Deuxième chaîne 2", chaine);
    printf("%s\n", chaine);
}

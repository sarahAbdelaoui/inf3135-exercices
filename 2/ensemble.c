#include <stdio.h>
#include <stdbool.h>

/**
 * Affiche l'ensemble encodé par l'entier positif i.
 */
void afficherEnsemble(unsigned int i) {
    printf("{");
    int e = 0;
    bool premier = true;
    while (i != 0) {
        if (i % 2 == 1) {
            if (premier) premier = false;
            else printf(",");
            printf("%d", e);
        }
        ++e;
        i /= 2;
    }
    printf("}");
}

int main() {
    unsigned int i;
    for (i = 0; i < 20; ++i) {
        afficherEnsemble(i);
        printf("\n");
    }
}

